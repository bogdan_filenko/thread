using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Password;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PasswordController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly EmailService _emailService;

        public PasswordController(UserService userService, EmailService emailService)
        {
            _userService = userService;
            _emailService = emailService;
        }

        [HttpPost("email")]
        public ActionResult SendResetMessage([FromQuery] string targetEmail)
        {
            var userId = this.GetUserIdFromToken();
            _emailService.SendPasswordResetMessage(targetEmail, userId);
            
            return Ok();
        }

        [HttpPut("reset")]
        public async Task<ActionResult> ResetPassword([FromBody] PasswordResetDTO pwdResetDto)
        {
            await _userService.UpdatePassword(pwdResetDto);
            return Ok();
        }
    }
}
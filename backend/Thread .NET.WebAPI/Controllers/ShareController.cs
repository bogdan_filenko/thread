using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Thread_.NET.BLL.Services;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.WebAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("/api/[controller]")]
    public class ShareController : ControllerBase
    {
        private readonly EmailService _emailService;

        public ShareController(EmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("email")]
        public async Task<ActionResult> SharePostByEmail([FromBody] PostDTO postDTO, [FromQuery] string targetEmail, [FromQuery] string sender)
        {
            await _emailService.SharePost(postDTO, targetEmail, sender);
            return Ok();
        }
    }
}
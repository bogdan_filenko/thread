﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Hubs;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.Comment;
using Microsoft.EntityFrameworkCore;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<ReactionsHub> _reactionsHub;
        private readonly EmailService _emailService;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<ReactionsHub> reactionsHub, EmailService emailService) : base(context, mapper) 
        {
            _reactionsHub = reactionsHub;
            _emailService = emailService;
        }

        public async Task ChangePostReaction(NewReactionDTO reaction)
        {
            var reactions = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);
            var reactionPost = await _context.Posts
                .Include(p => p.Preview)
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Reactions)
                    .ThenInclude(r => r.User)
                        .ThenInclude(u => u.Avatar)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Author)
                .FirstAsync(p => p.Id == reaction.EntityId);

            if (reactions.Any())
            {
                _context.PostReactions.RemoveRange(reactions);

                if (reactions.FirstAsync().Result.IsLike != reaction.IsLike)
                {
                    _context.PostReactions.Add(_mapper.Map<PostReaction>(reaction));
                }
                await _context.SaveChangesAsync();
                
                await _reactionsHub.Clients.All.SendAsync("PostReactionChanged", _mapper.Map<PostDTO>(reactionPost));

                return;
            }

            _context.PostReactions.Add(_mapper.Map<PostReaction>(reaction));
            await _context.SaveChangesAsync();

            if (reaction.IsLike && reaction.UserId != reactionPost.AuthorId)
            {
                await _emailService.SendReactionMessage(reaction.UserId, reactionPost);
            }

            await _reactionsHub.Clients.All.SendAsync("PostReactionChanged", _mapper.Map<PostDTO>(reactionPost), (await _context.Users.FirstAsync(u => u.Id == reaction.UserId)).UserName);
        }

        public async Task ChangeCommentReaction(NewReactionDTO reaction)
        {
            var reactions = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);
            var reactionComment = await _context.Comments
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Reactions)
                    .ThenInclude(r => r.User)
                .FirstAsync(p => p.Id == reaction.EntityId);

            if (reactions.Any())
            {
                _context.CommentReactions.RemoveRange(reactions);

                if (reactions.FirstAsync().Result.IsLike != reaction.IsLike)
                {
                    _context.CommentReactions.Add(_mapper.Map<CommentReaction>(reaction));
                }
                await _context.SaveChangesAsync();
                
                await _reactionsHub.Clients.All.SendAsync("CommentReactionChanged", _mapper.Map<CommentDTO>(reactionComment));

                return;
            }

            _context.CommentReactions.Add(_mapper.Map<CommentReaction>(reaction));
            await _context.SaveChangesAsync();

            await _reactionsHub.Clients.All.SendAsync("CommentReactionChanged", _mapper.Map<CommentDTO>(reactionComment));
        }
    }
}

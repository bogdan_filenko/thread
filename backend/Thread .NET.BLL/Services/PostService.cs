﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using System;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                        .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                    .ThenInclude(reaction => reaction.User)
                            .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(author => author.Avatar)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task UpdatePost(PostUpdateDTO postDto)
        {
            Post newPost = _mapper.Map<Post>(postDto);
            Post updatablePost = await _context.Posts.FirstAsync(p => p.Id == postDto.Id);

            updatablePost.Body = newPost.Body;
            updatablePost.Preview = newPost.Preview;
            updatablePost.UpdatedAt = DateTime.Now;

            await _context.SaveChangesAsync();

            var updatedPostDTO = _mapper.Map<PostDTO>(updatablePost);
            await _postHub.Clients.All.SendAsync("PostUpdated", updatedPostDTO);
        }

        public async Task DeletePost(int postId)
        {
            _context.Posts.Remove(await _context.Posts.FirstAsync(p => p.Id == postId));
            await _context.SaveChangesAsync();

            await _postHub.Clients.All.SendAsync("PostDeleted", postId);
        }
    }
}

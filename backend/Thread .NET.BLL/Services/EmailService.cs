using AutoMapper;
using System.Threading.Tasks;
using System.Net.Mail;
using System;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Entities;
using Thread_.NET.DAL.Context;
using Microsoft.Extensions.Configuration;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Services
{
    public class EmailService : BaseService
    {
        private readonly SmtpClient _smtpClient;
        public EmailService(ThreadContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper)
        {
            _smtpClient = new SmtpClient("smtp.gmail.com", 587);

            _smtpClient.Credentials = new NetworkCredential(configuration["EmailCredential"], configuration["PasswordCredential"]);
            _smtpClient.UseDefaultCredentials = false;
            _smtpClient.EnableSsl = true;
        }
        public async Task SendReactionMessage(int senderId, Post post)
        {
            try
            {
                var sender = await _context.Users.FirstAsync(user => user.Id == senderId);

                MailMessage message = new MailMessage(sender.Email, post.Author.Email, $"{sender.UserName} liked your post",
                    $"<p style=\"font-size: 17px;\">{sender.UserName} liked your post published in {post.CreatedAt}</p>" + post.ToEmailHtml());
                message.IsBodyHtml = true;

                _smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task SharePost(PostDTO postDto, string targetEmail, string sender)
        {
            string senderEmail = (await _context.Users.FirstAsync(u => u.UserName == sender)).Email;
            var post = _mapper.Map<Post>(postDto);

            MailMessage message = new MailMessage(senderEmail, targetEmail, $"{sender} shared you a post",
                $"<p style=\"font-size: 17px;\">{sender} shared you a post of {post.Author.UserName} published in {post.CreatedAt}</p>" + post.ToEmailHtml());
            message.IsBodyHtml = true;

            _smtpClient.Send(message);
        }

        public void SendPasswordResetMessage(string targetEmail, int userId)
        {
            MailMessage message = new MailMessage("threadnet2@gmail.com", targetEmail, "Password reset",
                "Follow the link to reset your password:\n" +
                $"\thttp://localhost:4200/password/reset/{userId}");

            _smtpClient.Send(message);
        }
    }
}
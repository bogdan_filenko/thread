﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Hubs;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper)
        {
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .Include(comment => comment.Reactions)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var createdCommentDTO = _mapper.Map<CommentDTO>(createdComment);
            await _commentHub.Clients.All.SendAsync("NewComment" ,createdCommentDTO);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentUpdateDTO commentUpdate)
        {
            var comment = _mapper.Map<Comment>(commentUpdate);

            var updatableComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(c => c.Id == commentUpdate.Id);

            updatableComment.Body = comment.Body;

            await _context.SaveChangesAsync();

            var updatedCommentDTO = _mapper.Map<CommentDTO>(updatableComment);
            await _commentHub.Clients.All.SendAsync("CommentUpdated", updatedCommentDTO);
        }

        public async Task DeleteComment(int commentId)
        {
            _context.Comments.Remove(await _context.Comments.FirstAsync(c => c.Id == commentId));
            await _context.SaveChangesAsync();

            await _commentHub.Clients.All.SendAsync("CommentDeleted", commentId);
        }
    }
}

using System;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class AlreadyExistsException : Exception
    {
        public AlreadyExistsException(string name) : base($"Entity {name} already exists")
        {
            
        }
    }
}
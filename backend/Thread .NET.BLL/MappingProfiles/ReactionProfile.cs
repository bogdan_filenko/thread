﻿using AutoMapper;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Entities.Abstract;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.MappingProfiles
{
    public sealed class ReactionProfile : Profile
    {
        public ReactionProfile()
        {
            CreateMap<Reaction, ReactionDTO>();
            
            CreateMap<ReactionDTO, Reaction>();

            CreateMap<NewReactionDTO, PostReaction>()
                .ForMember(dest => dest.PostId, src => src.MapFrom(s => s.EntityId));

            CreateMap<NewReactionDTO, CommentReaction>()
                .ForMember(dest => dest.CommentId, src => src.MapFrom(s => s.EntityId));
        }
    }
}

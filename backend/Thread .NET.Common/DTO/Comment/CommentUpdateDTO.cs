using System;
using System.Collections.Generic;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentUpdateDTO
    {
        public int Id { get; set; }
        public string Body { get; set; }
    }
}
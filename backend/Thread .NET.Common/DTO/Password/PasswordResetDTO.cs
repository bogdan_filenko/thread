namespace Thread_.NET.Common.DTO.Password
{
    public sealed class PasswordResetDTO
    {
        public int Id { get; set; }
        public string Password { get; set; }
    }
}

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommentUpdate } from 'src/app/models/comment/comment-update';

@Component({
    selector: 'app-update-comment',
    templateUrl: './update-comment.component.html',
    styleUrls: ['./update-comment.component.sass', './../../app.component.sass']
})
export class UpdateCommentComponent {
    @Input() public commentUpdate: CommentUpdate;

    @Output() public onUpdated = new EventEmitter<CommentUpdate>();

    public updateComment() {
        this.onUpdated.emit(this.commentUpdate);
    }
}

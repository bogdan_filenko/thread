import { Component, Input, Output, EventEmitter } from "@angular/core";
import { PostUpdate } from '../../models/post/post-update';
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: 'app-update-post',
    templateUrl: './update-post.component.html',
    styleUrls: ['./update-post.component.sass', './../../app.component.sass']
})

export class UpdatePostComponent {
    @Input() public postUpdate: PostUpdate;

    @Output() public onUpdated = new EventEmitter<[postUpdate: PostUpdate, imageFile: File]>();

    public imageFile: File;

    constructor(private snackBarService: SnackBarService) { }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.postUpdate.previewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.postUpdate.previewImage = undefined;
        this.imageFile = undefined;
    }

    public updatePost() {
        this.onUpdated.emit([this.postUpdate, this.imageFile]);
    }
}
import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Reaction } from "src/app/models/reactions/reaction";

@Component({
    templateUrl: './likes-dialog.component.html',
    styleUrls: ['./likes-dialog.component.sass']
})

export class LikesDialogComponent {
    public likes: Reaction[];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.likes = data.postLikes;
    }
}
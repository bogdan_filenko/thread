import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { PostUpdate } from 'src/app/models/post/post-update';
import { HubProvider } from 'src/app/services/hub.service';
import { LikesDialogService } from 'src/app/services/likes-dialog.service';
import { ShareDialogService } from 'src/app/services/share-dialog.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showUpdate = false;
    public showComments = false;
    public newComment = {} as NewComment;
    public postUpdate: PostUpdate;

    public likesNumber: number;
    public dislikesNumber: number

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private postService: PostService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private gyazoService: GyazoService,
        private hubProvider: HubProvider,
        private likesDialogService: LikesDialogService,
        private shareDialogService: ShareDialogService
    ) { }

    public ngOnInit() {
        this.hubProvider.registerPostReactionsHubActions(
            ["PostReactionChanged", (post, sender) => {
                if (this.post.id === post.id) {
                    if (sender && this.currentUser.userName !== sender && this.post.author.userName !== sender) {
                        this.snackBarService.showReactionMessage(this.post.previewImage, sender);
                    }
                    this.post = post;

                    this.likesNumber = this.post.reactions.filter(r => r.isLike).length;
                    this.dislikesNumber = this.post.reactions.length - this.likesNumber;
                }
            }]
        );

        this.hubProvider.registerCommentHubActions(
            ["CommentUpdated", (comment: Comment) => {
                if (comment) {
                    this.updateComment(comment);
                    this.snackBarService.showUsualMessage("The comment has been updated");
                }
            }],
            ["CommentDeleted", (commentId: number) => {
                if (commentId) {
                    this.deleteComment(commentId);
                    this.snackBarService.showUsualMessage("The comment has been deleted");
                }
            }]
        );

        this.sortCommentArray(this.post.comments);

        this.likesNumber = this.post.reactions.filter(r => r.isLike).length;
        this.dislikesNumber = this.post.reactions.length - this.likesNumber;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.showUpdate = false;
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }
        this.showUpdate = false;
        this.showComments = !this.showComments;
    }

    public openUpdateDialog() {
        if (this.showUpdate) {
            this.showUpdate = false;
        }
        else {
            this.showComments = false;
            this.postUpdate = {
                id: this.post.id,
                body: this.post.body,
                previewImage: this.post.previewImage
            };

            this.showUpdate = true;
        }
    }

    public updatePost(updateInfo: [PostUpdate, File]) {
        this.postService.updatePost(updateInfo[0]).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            if (updateInfo[1]) {
                this.gyazoService.uploadImage(updateInfo[1]);
            }
            this.post.body = updateInfo[0].body;
            this.post.previewImage = updateInfo[0].previewImage;

            this.snackBarService.showUsualMessage(`The post has been successfully updated`);
            this.showUpdate = false;
        },
            (error) => this.snackBarService.showErrorMessage(error));
    }

    public deletePost() {
        this.postService.removePost(this.post.id).subscribe(() => {
            this.snackBarService.showUsualMessage(`The post has been successfully deleted`);
        },
            (error) => this.snackBarService.showErrorMessage(error));
    }

    public setPostReaction(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.changePostReaction(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;

                    this.likesNumber = post.reactions.filter(r => r.isLike).length;
                    this.dislikesNumber = post.reactions.length - this.likesNumber;
                });

            return;
        }

        this.likeService
            .changePostReaction(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;

                this.likesNumber = post.reactions.filter(r => r.isLike).length;
                this.dislikesNumber = post.reactions.length - this.likesNumber;
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public updateComment(updatedComment: Comment) {
        let oldComment = this.post.comments.find(c => c.id === updatedComment.id);

        if (oldComment) {
            let commentIndex = this.post.comments.indexOf(oldComment);
            this.post.comments[commentIndex].body = updatedComment.body;
        }
    }

    public deleteComment(commentId: number) {

        let comment = this.post.comments.find(c => c.id === commentId);

        if (comment) {
            let commentIndex = this.post.comments.indexOf(comment);
            this.post.comments = this.post.comments.slice(0, commentIndex)
                .concat(this.post.comments.slice(commentIndex + 1, this.post.comments.length));
        }
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public openLikesDialog() {
        this.likesDialogService.openLikesDialog(this.post.reactions.filter(r => r.isLike));
    }

    public openShareDialog() {
        this.shareDialogService.openAuthDialog(this.post, this.currentUser.userName);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { HubProvider } from 'src/app/services/hub.service';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass', './../../app.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isMyFavourites = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private hubProvider: HubProvider
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();

        this.hubProvider.stopHubs();
    }

    public ngOnInit() {
        this.getPosts();
        this.getUser();

        this.hubProvider.connectHubs();
        this.hubProvider.registerPostHubActions(
            ["NewPost", (newPost: Post) => {
                if (newPost) {
                    this.addNewPost(newPost);
                }
            }],
            ["PostUpdated", (post: Post) => {
                if (post) {
                    this.updatePost(post);
                }
            }],
            ["PostDeleted", (postId: number) => {
                if (postId) {
                    this.deletePost(postId);
                }
            }]
        );

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.snackBarService.showUsualMessage(`The post has been successfully added`);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public onlyMineSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = true;
            this.posts = this.isMyFavourites
                ? this.cachedPosts.filter((x) => x.reactions.find(r => r.user.id === this.currentUser.id && r.isLike))
                : this.cachedPosts;
            this.posts = this.posts.filter((x) => x.author.id === this.currentUser.id);
        } else {
            this.isOnlyMine = false;
            this.posts = this.isMyFavourites
                ? this.cachedPosts.filter((x) => x.reactions.find(r => r.user.id === this.currentUser.id && r.isLike))
                : this.cachedPosts;
        }
    }

    public favouritesSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isMyFavourites = true;
            this.posts = this.isOnlyMine
                ? this.cachedPosts.filter((x) => x.author.id === this.currentUser.id)
                : this.cachedPosts
            this.posts = this.posts.filter((x) => x.reactions.find(r => r.user.id === this.currentUser.id && r.isLike));
        } else {
            this.isMyFavourites = false;
            this.posts = this.isOnlyMine
                ? this.cachedPosts.filter((x) => x.author.id === this.currentUser.id)
                : this.cachedPosts
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    public updatePost(updatedPost: Post) {
        let oldPost = this.posts.find((p) => p.id == updatedPost.id);
        if (oldPost) {
            let oldPostIndex = this.posts.indexOf(oldPost);
            this.posts[oldPostIndex].body = updatedPost.body;
            this.posts[oldPostIndex].previewImage = updatedPost.previewImage;

            this.cachedPosts = this.posts;
        }
    }

    public deletePost(postId: number) {
        let post = this.posts.find((p) => p.id == postId);
        if (post) {
            let postIndex = this.posts.indexOf(post);
            this.posts = this.posts.slice(0, postIndex).concat(this.posts.slice(postIndex + 1, this.posts.length));

            this.cachedPosts = this.posts;
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}

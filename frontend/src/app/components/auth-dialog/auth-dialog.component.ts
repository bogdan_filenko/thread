import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil, switchMap, map } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GyazoService } from 'src/app/services/gyazo.service';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;
    public email: string;
    public defaultAvatar = 'https://avatars.mds.yandex.net/get-ott/374297/2a000001616b87458162c9216ccd5144e94d/orig';
    public avatarImage: File;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    public authForm: FormGroup;

    constructor(
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService,
        private gyazoService: GyazoService
    ) {
        this.authForm = new FormGroup({
            "email": new FormControl("", [
                Validators.required,
                Validators.email
            ]),
            "userName": new FormControl("", Validators.required),
            "password": new FormControl("", [
                Validators.required,
                Validators.pattern('\\S{8,}')
            ])
        });
    }

    public ngOnInit() {
        this.avatar = this.defaultAvatar;
        this.title = this.data.dialogType === DialogType.SignIn ? 'Sign In' : 'Sign Up';
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public signUp() {
        this.gyazoService.uploadImage(this.avatarImage).pipe(
            map((imageData) => {
                this.avatar = imageData.url;
                return;
            })
        );
        this.authService.register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public uploadAvatar(target: any) {
        this.avatarImage = target.files[0];

        if (!this.avatarImage) {
            target.value = '';
            return;
        }

        if (this.avatarImage.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.avatar = reader.result as string));
        reader.readAsDataURL(this.avatarImage);
    }

    public removeAvatar() {
        this.avatarImage = undefined;
        this.avatar = this.defaultAvatar;
    }
}

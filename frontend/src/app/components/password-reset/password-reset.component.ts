import { Component, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { PasswordResetService } from "src/app/services/password-reset.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.sass']
})

export class PasswordResetComponent implements OnDestroy {
    public password: string;
    public passwordRepeat: string;

    private userId: number;

    public passwordResetGroup: FormGroup;

    public hidePass = true;
    public hidePassRepeat = true;

    private unsubscribe$ = new Subject<void>();

    private routerSubscription: Subscription;

    public constructor(
        private passwordResetService: PasswordResetService,
        private snackBarService: SnackBarService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.passwordResetGroup = new FormGroup({
            "password": new FormControl("", [
                Validators.required,
                Validators.pattern('\\S{8,}')
            ]),
            "passwordRepeat": new FormControl("", [
                Validators.required
            ])
        });

        this.routerSubscription = route.params.subscribe(params => this.userId = params['id']);
    }

    public resetPassword() {
        this.passwordResetService.resetPassword({
            id: this.userId,
            password: this.password
        })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.snackBarService.showUsualMessage('The password has been successfully updated');

                this.router.navigate(['']);
            },
                (error) => (this.snackBarService.showErrorMessage(error)));
    }

    public backToHomePage() {
        this.router.navigate(['']);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();

        this.routerSubscription.unsubscribe();
    }
}
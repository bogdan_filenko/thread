import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialogType } from '../../models/common/auth-dialog-type';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { take, takeUntil } from 'rxjs/operators';
import { PasswordResetService } from 'src/app/services/password-reset.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public authorizedUser: User;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private authDialogService: AuthDialogService,
        private authService: AuthenticationService,
        private eventService: EventService,
        private userService: UserService,
        private router: Router,
        private passwordResetService: PasswordResetService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnInit() {
        this.getUser();

        this.eventService.userChangedEvent$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.authorizedUser = user ? this.userService.copyUser(user) : undefined));
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public logout() {
        this.authService.logout();
        this.authorizedUser = undefined;
        this.router.navigate(['/']);
    }

    public openAuthDialog(type: DialogType) {
        this.authDialogService.openAuthDialog(type);
    }

    public sendPasswordReset() {
        this.passwordResetService.sendResetEmail(this.authorizedUser.email)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.snackBarService.showUsualMessage(`Reseting link was send to ${this.authorizedUser.email}`);
            },
                (error) => (this.snackBarService.showErrorMessage(error)));
    }

    private getUser() {
        if (!this.authService.areTokensExist()) {
            return;
        }

        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.authorizedUser = this.userService.copyUser(user)));
    }
}

import { Component, Inject, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ShareDialogType } from "src/app/models/common/share-dialog-type";
import { Post } from "src/app/models/post/post";
import { ShareService } from "src/app/services/share.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: 'app-share-dialog',
    templateUrl: './share-dialog.component.html',
    styleUrls: ['./share-dialog.component.sass']
})

export class ShareDialogComponent implements OnDestroy {
    public shareDialogType = ShareDialogType;
    public selectedDialogType: ShareDialogType = ShareDialogType.None;

    public shareGroup: FormGroup;
    public post: Post;
    public sender: string;

    public targetEmail: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<ShareDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private shareService: ShareService,
        private snackBarService: SnackBarService
    ) {
        this.post = data.post;
        this.sender = data.sender;

        this.shareGroup = new FormGroup({
            "email": new FormControl("", [
                Validators.required,
                Validators.email
            ])
        });
    }

    public changeDialogType(dialogType: ShareDialogType) {
        this.selectedDialogType = dialogType;
    }

    public sharePostByEmail() {
        this.shareService.sharePostByEmail(this.post, this.sender, this.targetEmail)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.dialogRef.close();

                this.snackBarService.showUsualMessage('The post was succesfully shared');
            },
                (error) => (this.snackBarService.showErrorMessage(error)));
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
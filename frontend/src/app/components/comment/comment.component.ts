import { Component, Input, OnInit } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { CommentUpdate } from 'src/app/models/comment/comment-update';
import { User } from 'src/app/models/user';
import { CommentService } from 'src/app/services/comment.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { empty, Observable, Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HubProvider } from 'src/app/services/hub.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { LikeService } from 'src/app/services/like.service';
import { LikesDialogService } from 'src/app/services/likes-dialog.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public showUpdate = false;
    public commentUpdate: CommentUpdate;

    public likesNumber: number;
    public dislikesNumber: number;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private commentService: CommentService,
        private hubProvider: HubProvider,
        private likeService: LikeService,
        private snackBarService: SnackBarService,
        private likesDialogService: LikesDialogService
    ) { }

    public ngOnInit() {
        this.hubProvider.registerCommentReactionsHubActions(
            ["CommentReactionChanged", (comment) => {
                if (this.comment.id === comment.id) {
                    this.comment = comment;

                    this.likesNumber = this.comment.reactions.filter(r => r.isLike).length;
                    this.dislikesNumber = this.comment.reactions.length - this.likesNumber;
                }
            }]
        );

        this.likesNumber = this.comment.reactions.filter(r => r.isLike).length;
        this.dislikesNumber = this.comment.reactions.length - this.likesNumber;
    }

    public openUpdateDialog() {
        if (this.showUpdate) {
            this.showUpdate = false;
        }
        else {
            this.commentUpdate = {
                id: this.comment.id,
                body: this.comment.body,
            };

            this.showUpdate = true;
        }
    }

    public setCommentReaction(isLike: boolean) {
        this.likeService
            .changeCommentReaction(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;

                this.likesNumber = comment.reactions.filter(r => r.isLike).length;
                this.dislikesNumber = comment.reactions.length - this.likesNumber;
            });
    }

    public openLikesDialog() {
        this.likesDialogService.openLikesDialog(this.comment.reactions.filter(r => r.isLike));
    }

    public updateComment(commentUpdate: CommentUpdate) {
        this.commentService.updateComment(commentUpdate).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            this.showUpdate = false;
        },
            (error) => this.snackBarService.showErrorMessage(error));
    }

    public deleteComment() {
        this.commentService.deleteComment(this.comment.id).subscribe(() => { },
            (error) => this.snackBarService.showErrorMessage(error));
    }
}

import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ShareDialogComponent } from "../components/share/share-dialog.component";
import { Post } from "../models/post/post";

@Injectable({ providedIn: 'root' })
export class ShareDialogService {

    public constructor(
        private dialog: MatDialog
    ) { }

    public openAuthDialog(post: Post, sender: string) {
        this.dialog.open(ShareDialogComponent, {
            data: {
                post: post,
                sender: sender
            },
            minWidth: 300,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '150px'
            }
        });
    }
}
import { Injectable } from '@angular/core';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(
        private postService: PostService,
        private commentService: CommentService) { }

    private createNewReaction(entity: Post | Comment, currentUser: User, isLike: boolean): NewReaction {
        return {
            entityId: entity.id,
            isLike: isLike,
            userId: currentUser.id
        };
    }

    private updateCurrentArray(entity: Post | Comment, currentUser: User, hasReaction: boolean, isLike: boolean): Reaction[] {
        if (hasReaction) {
            let userReaction = entity.reactions.find((x) => x.user.id === currentUser.id);

            return userReaction.isLike === isLike
                ? entity.reactions.filter((x) => x.user.id !== currentUser.id)
                : entity.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: isLike, user: currentUser });
        }
        return entity.reactions.concat({ isLike: isLike, user: currentUser });
    }

    public changePostReaction(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        innerPost.reactions = this.updateCurrentArray(innerPost, currentUser, hasReaction, isLike);
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        let reaction: NewReaction = this.createNewReaction(innerPost, currentUser, isLike);

        return isLike ? this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = this.updateCurrentArray(innerPost, currentUser, hasReaction, isLike);

                return of(innerPost);
            })
        ) : this.postService.dislikePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = this.updateCurrentArray(innerPost, currentUser, hasReaction, isLike);

                return of(innerPost);
            })
        );
    }

    public changeCommentReaction(comment: Comment, currentUser: User, isLike: boolean) {
        const innerComment = comment;

        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        innerComment.reactions = this.updateCurrentArray(innerComment, currentUser, hasReaction, isLike);
        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        let reaction: NewReaction = this.createNewReaction(innerComment, currentUser, isLike);

        return isLike ? this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = this.updateCurrentArray(innerComment, currentUser, hasReaction, isLike);

                return of(innerComment);
            })
        ) : this.commentService.dislikeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = this.updateCurrentArray(innerComment, currentUser, hasReaction, isLike);

                return of(innerComment);
            })
        );
    }
}

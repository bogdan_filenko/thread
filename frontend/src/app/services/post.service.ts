import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { PostUpdate } from '../models/post/post-update';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public updatePost(postUpdate: PostUpdate) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, postUpdate);
    }

    public removePost(postId: number) {
        return this.httpService.deleteRequest<Post>(`${this.routePrefix}/${postId}`);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/dislike`, reaction)
    }
}

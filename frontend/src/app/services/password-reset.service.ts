import { Injectable } from "@angular/core";
import { PasswordReset } from "../models/password/password-reset";
import { User } from "../models/user";
import { HttpInternalService } from "./http-internal.service";

@Injectable({ providedIn: 'root' })
export class PasswordResetService {
    private readonly routePrefix = '/api/password';

    public constructor(
        private httpInternalService: HttpInternalService
    ) { }

    public sendResetEmail(targetEmail: string) {
        return this.httpInternalService.postFullRequest<User>(`${this.routePrefix}/email?targetEmail=${targetEmail}`, {});
    }

    public resetPassword(passwordReset: PasswordReset) {
        return this.httpInternalService.putFullRequest<PasswordReset>(`${this.routePrefix}/reset`, passwordReset);
    }
}
import { Injectable, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LikeSnackbarComponent } from '../components/like-snackbar/like-snackbar.component';

@Injectable({ providedIn: 'root' })
export class SnackBarService {
    public constructor(private snackBar: MatSnackBar) { }

    public showErrorMessage(error: any) {
        this.snackBar.open(error, '', { duration: 3000, panelClass: 'error-snack-bar' });
    }

    public showUsualMessage(message: any) {
        this.snackBar.open(message, '', { duration: 3000, panelClass: 'usual-snack-bar' });
    }

    public showReactionMessage(imageUrl: string, sender: string) {
        this.snackBar.openFromComponent(LikeSnackbarComponent, {
            duration: 3000,
            panelClass: 'usual-snack-bar',
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            data: {
                imageUrl: imageUrl,
                sender: sender
            }
        })
    }
}

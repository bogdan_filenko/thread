import { Injectable } from '@angular/core';
import { SnackBarService } from './snack-bar.service';

@Injectable()
export class ImageService {
    private imageFile: File;
    private imageUrl: string;

    constructor(private snackBarService: SnackBarService ) {}
    
    public loadImage(target: any): [imageUrl: string, imageFile: File] {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => {
            this.imageUrl = reader.result as string;
        });
        reader.readAsDataURL(this.imageFile);
        
        return [this.imageUrl, this.imageFile];
    }
}

import { Injectable } from '@angular/core';
import { LikesDialogComponent } from '../components/likes-dialog/likes-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class LikesDialogService {

    public constructor(private dialog: MatDialog) { }

    public openLikesDialog(likes: Reaction[]) {
        this.dialog.open(LikesDialogComponent, {
            data: { postLikes: likes },
            minWidth: 300,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });
    }
}

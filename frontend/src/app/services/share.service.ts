import { Injectable } from "@angular/core";
import { Post } from "../models/post/post";
import { HttpInternalService } from "./http-internal.service";

@Injectable({ providedIn: 'root' })
export class ShareService {
    private readonly routePrefix = '/api/share'

    constructor(
        private httpInternalService: HttpInternalService
    ) { }

    public sharePostByEmail(post: Post, sender: string, targetEmail: string) {
        return this.httpInternalService.postFullRequest<Post>(`${this.routePrefix}/email?sender=${sender}&targetEmail=${targetEmail}`,
            post);
    }
}
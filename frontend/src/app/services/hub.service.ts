import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { SnackBarService } from "./snack-bar.service";

type HubAction = [action: string, callback: (...args: any[]) => void];

@Injectable({ providedIn: 'root' })
export class HubProvider {
    private postHub: HubConnection;
    private reactionsHub: HubConnection;
    private commentHub: HubConnection;

    private isPostHubRegistered = false;
    private isPostReactionsHubRegistered = false;
    private isCommentHubRegistered = false;
    private isCommentReactionsHubRegistered = false;

    constructor(private snackBarService: SnackBarService) { }

    public connectHubs() {
        this.postHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/post`).build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.reactionsHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/reactions`).build();
        this.reactionsHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/comment`).build();
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));
    }

    public registerPostHubActions(...actions: HubAction[]) {
        if (!this.isPostHubRegistered) {
            for (let action of actions) {
                this.postHub.on(action[0], action[1]);
            }
            this.isPostHubRegistered = true;
        }
    }

    public registerPostReactionsHubActions(...actions: HubAction[]) {
        if (!this.isPostReactionsHubRegistered) {
            for (let action of actions) {
                this.reactionsHub.on(action[0], action[1]);
            }
            this.isPostReactionsHubRegistered = true;
        }
    }

    public registerCommentHubActions(...actions: HubAction[]) {
        if (!this.isCommentHubRegistered) {
            for (let action of actions) {
                this.commentHub.on(action[0], action[1]);
            }
            this.isCommentHubRegistered = true;
        }
    }

    public registerCommentReactionsHubActions(...actions: HubAction[]) {
        if (!this.isCommentReactionsHubRegistered) {
            for (let action of actions) {
                this.reactionsHub.on(action[0], action[1]);
            }
            this.isCommentReactionsHubRegistered = true;
        }
    }

    public stopHubs() {
        this.postHub.stop();
        this.reactionsHub.stop();
        this.commentHub.stop();

        this.isPostHubRegistered = false;
        this.isCommentHubRegistered = false;
        this.isPostReactionsHubRegistered = false;
        this.isCommentReactionsHubRegistered = false;
    }
}
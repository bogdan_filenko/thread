export enum ShareDialogType {
    None = 0,
    Email = 1,
    Link = 2
}
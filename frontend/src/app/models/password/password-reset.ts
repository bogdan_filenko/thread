export interface PasswordReset {
    id: number;
    password: string;
}
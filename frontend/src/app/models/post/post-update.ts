export interface PostUpdate {
    id: number;
    previewImage: string;
    body: string;
}
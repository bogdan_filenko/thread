import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainThreadComponent } from './components/main-thread/main-thread.component';
import { PostComponent } from './components/post/post.component';
import { HomeComponent } from './components/home/home.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthDialogComponent } from './components/auth-dialog/auth-dialog.component';
import { CommentComponent } from './components/comment/comment.component';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { UpdatePostComponent } from './components/update-post/update-post.component';
import { UpdateCommentComponent } from './components/update-comment/update-comment.component';
import { LikesDialogComponent } from './components/likes-dialog/likes-dialog.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { LikeSnackbarComponent } from './components/like-snackbar/like-snackbar.component';
import { ShareDialogComponent } from './components/share/share-dialog.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';

@NgModule({
    declarations: [
        AppComponent,
        MainThreadComponent,
        PostComponent,
        HomeComponent,
        UserProfileComponent,
        AuthDialogComponent,
        CommentComponent,
        UpdatePostComponent,
        UpdateCommentComponent,
        LikesDialogComponent,
        LikeSnackbarComponent,
        ShareDialogComponent,
        PasswordResetComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialComponentsModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        ReactiveFormsModule,
        ScrollingModule
    ],
    exports: [
        MaterialComponentsModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
